#!/bin/bash
yum -y install httpd

if rpm -qa | grep -i httpd >/dev/null 2>&1
then
  echo "HTTPD was installed successfully"
else
  echo "HTTPD was NOT installed successfully"
  exit 3
fi

systemctl start httpd
systemctl enable httpd

#cp /vagrant/bin/index.html /var/www/html
cp /vagrant/bin/script_rhel /etc/init.d
sudo chmod +x /etc/init.d/script_rhel
sudo chkconfig --add script_rhel
sudo service script_rhel start
