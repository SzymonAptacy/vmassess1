yum -y install haproxy nano

if rpm -qa | grep -i haproxy >/dev/null 2>&1
then
  echo "haproxy was installed successfully"
else
  echo "haproxy was NOT installed successfully"
  exit 5
fi

cp -f /vagrant/haproxy.cfg /etc/haproxy
systemctl enable haproxy
systemctl start haproxy
