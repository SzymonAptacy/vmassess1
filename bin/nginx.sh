#!/bin/bash
apt -y update
apt -y install nginx nano

if dpkg -l nginx | grep -i nginx >/dev/null 2>&1
then
  echo "NGINX was installed successfully"
else
  echo "NGINX was NOT installed successfully"
  exit 1
fi

cp /vagrant/bin/script_ubuntu /etc/init.d
sudo chmod +x /etc/systemd/system/script_ubuntu
sudo update-rc.d script_ubuntu enable
sudo service script_ubuntu start
